<?php

/**
 * Implementation of hook_features_export_options().
 */
function custom_teasers_views_features_export_options() {
  return custom_teasers_views_load_all_list();
}

/**
 * Implementation of hook_features_export_render().
 */
function custom_teasers_views_features_export_render($module = 'foo', $data) {
  $code = array();
  $code[] = '  $views = array();';
  $code[] = '';

  // Build views & add to export array
  foreach ($data as $view_name) {
    // Build the view
    $view = custom_teasers_views_load($view_name);
    if ($view) {
      $code[] = '  // Exported Teaser View: '. $view['title'];
      $code[] = '  $view = '. custom_teasers_views_var_export($view, '  ') .';';
      $code[] = '  $views[$view[\'name\']] = $view;';
      $code[] = '';
    }
  }
  $code[] = '  return $views;';
  $code = implode("\n", $code);

  return array('custom_teasers_views_default_views' => $code);
}

/**
 * Export var function -- from Views.
 *
 * @param mixed $var
 *   The variable to be exported.
 * @param string $prefix
 *   The string prefix for each line.
 *
 * @return string
 *   Returns the exported PHP code.
 */

function custom_teasers_views_var_export($var, $prefix = '') {
  $output = '';
  if (is_object($var)) {
    $output = '('. gettype($var) .') ';
    $var = (array) $var;
  }
  if (is_array($var)) {
    if (empty($var)) {
      $output .= 'array()';
    }
    else {
      $output .= "array(\n";
      foreach ($var as $key => $value) {
        $output .= "  '$key' => ". custom_teasers_views_var_export($value, '  ') .",\n";
      }
      $output .= ')';
    }
  }
  else if (is_bool($var)) {
    $output = $var ? 'TRUE' : 'FALSE';
  }
  elseif (is_int($var) || is_float($var)) {
    $output = (string) $var;
  }
  else {
    $output = var_export($var, TRUE);
  }

  if ($prefix) {
    $output = str_replace("\n", "\n$prefix", $output);
  }

  return $output;
}

/**
 * Implementation of hook_features_export().
 */
function custom_teasers_views_features_export($data, &$export, $module_name = '') {
  views_include('view');

  if (!isset($export['features']['custom_teasers_views'])) {
    $export['features']['custom_teasers_views'] = array();
  }

  // Build views & add to export array
  $views = array();
  foreach ($data as $view_name) {
    // Add to exports
    $export['features']['custom_teasers_views'][$view_name] = $view_name;

    // Build the view
    $view = custom_teasers_views_load($view_name);
    if ($view) {
      $views[$view_name] = $view;
    }
  }

  // Add views as a dependency
  $export['dependencies']['custom_teasers_views'] = 'custom_teasers_views';

  // Discover module dependencies
  // We need to find dependencies based on:
  // 1. handlers
  // 2. plugins (style plugins, display plugins)
  // 3. other... (e.g. imagecache display option for CCK imagefields) : (

  // Handlers
  $handlers = array('fields', 'filters', 'arguments', 'sort', 'relationships');
  $handler_dependencies = views_handler_dependencies();

  // Plugins
  // For now we only support dependency detection for a subset of all views plugins
  $plugins = array('display', 'style', 'row', 'access');
  $plugin_dependencies = views_plugin_dependencies();

  foreach ($views as $view_name => $view) {
    $v = views_get_view('custom_teasers_views_'. $view_name);
    $v->build();
    foreach ($v->display as $display) {
      if (is_object($display->handler) && method_exists($display->handler, 'get_option')) {

        // Handlers
        foreach ($handlers as $handler) {
          $items = $display->handler->get_option($handler);
          if (!empty($items)) {
            foreach ($items as $item) {
              $module = $handler_dependencies[$item['table']];
              $export['dependencies'][$module] = $module;
            }
          }
        }

        // Plugins
        foreach ($plugins as $plugin_type) {
          switch ($plugin_type) {
            case 'display':
              $plugin_name = $display->display_plugin;
              break;
            case 'access':
              $access = $display->handler->get_option("access");
              if (!empty($access['type']) && $access['type'] != 'none') {
                $plugin_name = $access['type'];
              }
              else {
                $plugin_name = '';
              }
              break;
            default:
              $plugin_name = $display->handler->get_option("{$plugin_type}_plugin");
              break;
          }
          if (!empty($plugin_name)) {
            $module = $plugin_dependencies[$plugin_type][$plugin_name];
            $export['dependencies'][$module] = $module;
          }
        }
      }
    }
  }

  foreach (module_implements('custom_teasers_views_default_views') as $m) {
    $m_default_views = module_invoke($m, 'custom_teasers_views_default_views');
    if (is_array($m_default_views)) {
      foreach ($m_default_views as $view_name => $view) {
        if (array_key_exists($view_name, $export['features']['custom_teasers_views']) && ($m != $module_name)) {
          $v = custom_teasers_views_load($view_name);
          if ($v && isset($v['default'])) {
            unset($export['features']['custom_teasers_views'][$view_name]);
            $tokens = array('!view_name' => $view_name, '!module' => $m);
            $export['conflicts']['status'][] = t('The view <strong>!view_name</strong> will not be exported with your feature because it is provided by <strong>!module</strong> which has been added as a dependency.', $tokens);
            $export['dependencies'][$m] = $m;
          }
        }
      }
    }
  }
}

/**
 * Implementation of hook_features_revert().
 *
 * @param $module
 * name of module to revert content for
 */
function custom_teasers_views_features_revert($module = NULL) {
  // Get default views from feature
  if (module_hook($module, 'custom_teasers_views_default_views')) {
    $default_views = module_invoke($module, 'custom_teasers_views_default_views');

    // Delete Normal / Overridden views that are defined in code
    foreach ($default_views as $default_view) {
      custom_teasers_views_delete($default_view);
    }
  }
  else {
    drupal_set_message(t('Could not load default views.'), 'error');
    return FALSE;
  }
  return TRUE;
}
